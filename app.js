var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var passport = require('passport');
var mongoose = require('mongoose');
var morgan = require('morgan');
var path = require('path');
var cors = require('cors');

require('./config/passport')(passport); // pass passport for configuration

app.use(morgan('combined'));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
//app.use(bodyParser());

app.use(session({
    secret: 'secret!',
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static('public'));

require('./routes/auth/local')(app, passport);
require('./routes/auth/facebook')(app, passport);
require('./routes/auth/twitter')(app, passport);
require('./routes/auth/google')(app, passport);

//const uri = "mongodb://mongo:27017";
const uri = "mongodb://localhost:27017/app";

mongoose.connect(uri);

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', function () {
  console.log('Mongoose default connection open to ' + uri);
});

// If the connection throws an error
mongoose.connection.on('error',function (err) {
  console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
  console.log('Mongoose default connection disconnected');
});


app.get('/', function(request, response) {
    //response.redirect('/signup');
    response.sendFile(path.join(__dirname, '/public/app/index.html'));
});

app.get('/profile', isLoggedIn, function (request, response) {
   response.send( { user: request.user } );
});

app.get('/logout', function (request, response) {
    request.logout();
    response.redirect('/');
});

function isLoggedIn(request, response, next) {
    if (request.isAuthenticated())
        return next();
    response.redirect('/');
}

app.listen(3000, function() {
    console.log('Example app listening on port 3000!');
});
