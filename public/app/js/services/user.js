/**
 * Created by ee12099 on 06-07-2017.
 */
app.factory('userService', function ($http) {
   return {
       async: function () {
           return $http.get('/api/users');
       }
   };
});