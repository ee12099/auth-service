/**
 * Created by ee12099 on 06-07-2017.
 */
app.factory('AuthService', ['$http', function($http) {
    var authService = {};
    authService.login = function(credentials) {
        return $http({
            method: 'POST',
            url: '/login',
            data: $.param(credentials),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function (response) {
            return response.data;
        });
    };
    return authService;
}]);