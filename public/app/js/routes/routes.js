/**
 * Created by ee12099 on 06-07-2017.
 */
app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '../app/views/signup.html',
            controller: 'SignupController'
        })

        .when('/signup', {
            templateUrl: '../app/views/signup.html',
            controller: 'SignupController'
        })

        .when('/login', {
            templateUrl: '../app/views/login.html',
            controller: 'LoginController'
        })

        .when('/users', {
            templateUrl: '../app/views/users.html',
            controller: 'UsersController'
        });
});

