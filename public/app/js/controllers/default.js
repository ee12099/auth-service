/**
 * Created by ee12099 on 06-07-2017.
 */
app.controller('DefaultController', function ($scope) {
    $scope.search = {
        submit: 'Search'
    };
    $scope.links = [
        'Sign In',
        'Sign In Again',
        'Just One More Time',
        'Sign In One Last Time'
    ];
});