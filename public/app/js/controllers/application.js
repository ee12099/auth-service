/**
 * Created by ee12099 on 07-07-2017.
 */
app.controller('ApplicationController', function ($scope) {
    $scope.currentUser = null;
    $scope.setCurrentUser = function (user) {
        $scope.currentUser = user;
    };
    $scope.$on('Login successful', function () {
        console.log('Login successful');
    });
    $scope.$on('Login failed', function () {
        console.log('Login failed');
    });
});