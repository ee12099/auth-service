/**
 * Created by ee12099 on 06-07-2017.
 */
app.controller('SignupController', function ($scope) {
    $scope.title = 'Sign Up';
    $scope.label = {
        user: 'Username',
        pass: 'Password'
    };
    $scope.checkbox = 'Remember me';
    $scope.submit = 'Sign Up';
    $scope.link = 'Sign In';
});