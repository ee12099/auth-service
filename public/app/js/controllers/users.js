/**
 * Created by ee12099 on 06-07-2017.
 */
app.controller('UsersController', function($scope, userService) {
    $scope.table = {
        headers: []
    };
    userService.async().then(function(response) {
        $scope.users = response.data;
        if (response.data != null) {
            var keys = Object.keys(response.data[0].local);
            for (var i = 0; i < keys.length; i++) {
                $scope.table.headers.push(keys[i]);
            };
            $scope.table.headers.reverse();
            console.log($scope.table.headers);
        }
    });
});