/**
 * Created by ee12099 on 06-07-2017.
 */
app.controller('LoginController', function ($scope, $rootScope, AuthService) {
   $scope.title = 'Sign In';
   $scope.label = {
       user: 'Username',
       pass: 'Password'
   };
   $scope.checkbox = 'Remember me!';
   $scope.button = {
       submit: 'Sign In'
   };
   $scope.submitForm = function(credentials) {
       AuthService.login(credentials).then(function (user) {
           $rootScope.$broadcast('Login successful');
           $scope.setCurrentUser(user);
       }, function () {
           $rootScope.$broadcast('Login failed');
       });
   };
});
