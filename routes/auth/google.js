/**
 * Created by ee12099 on 08-07-2017.
 */
var path = require('path');

module.exports = function (app, passport) {
    app.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email']}));

    /*app.get('/auth/google/callback', passport.authenticate('google', {
        successRedirect: '/profile',
        failureRedirect: '/'
    }));*/
    app.get('/auth/google/callback', passport.authenticate('google'), function (request, response) {
        console.log(request.headers);
        if (request.header('context') === 'login') {
            response.send(request.user);
        } else {
            response.redirect('http://localhost:4000/#!callback/google');
        }
    });

    app.get('/connect/google', passport.authorize('google', { scope: ['profile', 'email'] }));

    /*app.get('/connect/google/callback', passport.authorize('google', {
        successRedirect: '/profile',
        failureRedirect: '/'
    }));*/
    app.get('/connect/google/callback', passport.authorize('google'), function (request, response) {
        response.send(request.user);
    });

    app.get('/unlink/google', function (request, response) {
       var user = request.user;
       user.google.token = undefined;
       user.save(function (err) {
           //response.redirect('/profile');
           response.send(request.user);
       });
    });
};
