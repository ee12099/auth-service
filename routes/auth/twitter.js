/**
 * Created by ee12099 on 07-07-2017.
 */

module.exports = function (app, passport) {
    app.get('/auth/twitter', passport.authenticate('twitter'));

    /*app.get('/auth/twitter/callback', passport.authenticate('twitter', {
        successRedirect: '/profile',
        failureRedirect: '/'
    }));*/
    app.get('/auth/twitter/callback', passport.authenticate('twitter'), function (request, response) {
        console.log(request.headers);
        if (request.header('context') === 'login') {
            response.send(request.user);
        } else {
            //response.send(request.user);
            response.redirect('http://localhost:4000/#!callback/twitter');
        }
    });

    app.get('/connect/twitter', passport.authorize('twitter', { scope: 'email '}));

    /*app.get('/connect/twitter/callback', passport.authorize('twitter', {
        successRedirect: '/profile',
        failureRedirect: '/'
    }));*/
    app.get('/connect/twitter/callback', passport.authorize('twitter'), function (request, response) {
        if (request.header('context') === 'login') {
            console.log({user: request.user});
            response.send(request.user);
        } else {
            console.log('Hello');
            response.redirect('http://localhost:4000/#!callback/twitter');
        }
    });

    app.get('/unlink/twitter', function (request, response) {
       var user = request.user;
       user.twitter.token = undefined;
       user.save(function (err) {
           //response.redirect('/profile');
           response.send(request.user);
       });
    });
};