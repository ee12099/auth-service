/**
 * Created by ee12099 on 07-07-2017.
 */
var path = require('path');

module.exports = function(app, passport) {
    app.get('/signup', function (request, response) {
        response.sendFile(path.join(__dirname, '../../public/app/html/signup.html'));
    });

    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/profile',
        failureRedirect: '/',
        failureFlash: false
    }));

    app.get('/login', function (request, response) {
        response.sendFile(path.join(__dirname, '../../public/app/html/login.html'));
    });

    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/profile',
        failureRedirect: '/',
        failureFlash: false
    }));

    app.get('/connect/local', function(request, response) {
       response.redirect('/signup');
    });

    app.post('/connect/local', passport.authenticate('local-signup', {
        successRedirect: '/profile',
        failureRedirect: '/connect/local',
        failureFlash: false
    }));

    app.get('/unlink/local', function(request, response) {
        var user = request.user;
        user.local.username = undefined;
        user.local.password = undefined;
        user.save(function (err) {
            response.redirect('/profile');
        });
    })
};