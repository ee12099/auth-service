/**
 * Created by ee12099 on 07-07-2017.
 */

module.exports = function (app, passport) {
    app.get('/auth/facebook', passport.authenticate('facebook', { scope: ['email'] }));

    /*app.get('/auth/facebook/callback', passport.authenticate('facebook', {
        successRedirect: '/profile',
        failureRedirect: '/'
    }));*/
    
    app.get('/auth/facebook/callback', passport.authenticate('facebook'), function (request, response) {
        if (request.header('context') === 'login') {
            response.send(request.user);
        } else {
            response.redirect('http://localhost:4000/#!callback/facebook');
        }

    });

    app.get('/connect/facebook', passport.authorize('facebook', { scope: ['email'] }));

    /*app.get('/connect/facebook/callback', passport.authorize('facebook', {
        successRedirect: '/profile',
        failureRedirect: '/'
    }));
    */

    app.get('/connect/facebook/callback', passport.authorize('facebook'), function (request, response) {
        //response.send(request.user);
        if (request.header('context') === 'login') {
            response.send(request.user);
        } else {
            response.redirect('http://localhost:4000/#!callback/facebook');
        }
    });

    app.get('/unlink/facebook', function (request, response) {
        var user = request.user;
        user.facebook.token = undefined;
        user.save(function (err) {
            //response.redirect('/profile');
            response.send(request.user);
        });
    });
};
