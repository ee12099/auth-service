var router = require('express').Router;

router.route('user')
    .get(function (request, response) {
        User.find(function (err, users) {
            if (err) {
                response.send(err);
            } else {
                response.json(users);
            }
        })
    });

exports.Router = router;